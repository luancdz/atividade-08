#pragma once
#include "ProdutoDao.h"
#include <vector>

using namespace std;
class ProdutoController 
{
public:
	ProdutoController();
	~ProdutoController();
	void inicia();
	bool cadastrarProduto(Produto *produto);
	bool removerProduto(string id);
	vector<string> consultarProdutos();
	void alterarProduto(string id, Produto *produto);
};

