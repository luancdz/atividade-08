#pragma once
#include <iostream>
#include <string>

using namespace std;
class Produto
{
private:
	string id;
	string nome;
	string descricao;
	string quantidade;
	string preco;
	string dataVencimento;
	
public:
	Produto();
	void setId(string id);
	void setNome(string nome);
	void setDescricao(string descricao);
	void setQuantidade(string quantidade);
	void setPreco(string preco);
	void setDataVencimento(string dataVencimento);
	string getId();
	string getNome();
	string getDescricao();
	string getQuantidade();
	string getPreco();
	string getDataVencimento();
	~Produto();
};













