#include "stdafx.h"
#include "ProdutoController.h"
#include "ProdutoView.h"
#include "Produto.h"
#include "ProdutoDao.h"
#include <vector>
#include <iostream>

using namespace std;
ProdutoController::ProdutoController()
{
}


ProdutoController::~ProdutoController()
{
}

void ProdutoController::inicia()
{
	bool saida = false;
	ProdutoView view;
	while (!saida) {

	}

}


bool ProdutoController::cadastrarProduto(Produto *produto)
{
	try {
		if (produto->getId() == " ") {
			return false;
		}
		if (produto->getNome() == " ") {
			return false;
		}
		if (produto->getDescricao() == " ") {
			return false;
		}
		if (produto->getQuantidade() == " ") {
			return false;
		}
		if (produto->getPreco() == " ") {
			return false;
		}
		if (produto->getDataVencimento() == " ") {
			return false;
		}

		ProdutoDao *produtoDao = new ProdutoDao();
		produtoDao->insertProduto(produto);
		return true;
	}
	catch (...) {

	}
}

bool ProdutoController::removerProduto(string id)
{
	if (id == " ") {
		return false;
	}
	else {
		ProdutoDao *produtoDao = new ProdutoDao();
		produtoDao->removeProduto(id);
		return true;
	}
}

vector<string> ProdutoController::consultarProdutos()
{
	ProdutoDao *produtoDao = new ProdutoDao();
	vector<string> listaProdutos = produtoDao->consultarProduto();
	return listaProdutos;
}

void ProdutoController::alterarProduto(string id, Produto *produto)
{
	ProdutoDao *produtoDao = new ProdutoDao();
	bool valida;
	valida = produtoDao->alteraProduto(id, produto);
	if (valida) {
		cout << "Produto Alterado com sucesso! " << endl;
	}
	else {
		cout << "Produto nao alterado" << endl;
	}
}
