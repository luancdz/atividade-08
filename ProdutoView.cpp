#include "stdafx.h"
#include "ProdutoView.h"
#include <iostream>
#include "Produto.h"
#include <string>
#include <vector>
#include "ProdutoController.h"


using namespace std;
ProdutoView::ProdutoView()
{
}


ProdutoView::~ProdutoView()
{
}

void ProdutoView::menu()
{
	while (1) {
		cout << "\t\tMENU Atividade 8" << endl;
		cout << "Selecione a opcao desejada:" << endl;
		cout << "1 - Cadastrar Produto" << endl;
		cout << "2 - Remover produto por ID" << endl;
		cout << "3 - Consultar todos os produtos" << endl;
		cout << "4 - Alterar dados de algum produto" << endl;
		cout << "Qualquer valor diferente deste, sai do programa" << endl;
		int variavel = 0;
		cin >> variavel;
		cin.clear();

		ProdutoController *produtoCtrl = new ProdutoController();
		bool retorno;
		if (variavel == 1) {
			system("cls");
			Produto *produto = new Produto();
			produto = insereProduto();

			retorno = produtoCtrl->cadastrarProduto(produto);

			if (retorno) {
				cout << "Cadastrado com sucesso" << endl;
			}
			else {
				cout << "Produto nao cadastrado" << endl;
			}
		}
		else if (variavel == 2) {
			system("cls");
			removerProduto();
		}
		else if (variavel == 3) {
			system("cls");
			consultaProdutos();
		}
		else if (variavel == 4) {
			system("cls");
			Produto *produto = new Produto();
			produto = insereProduto();
			string id;
			cout << "Insira o ID do produto desejado: " << endl;
			getline(cin, id);

			produtoCtrl->alterarProduto(id, produto);
		}
		else {
			exit(0);
		}
	}
}


void ProdutoView::removerProduto()
{
	string id;
	cout << "Informe o ID do produto: " << endl;
	cin >> id;
	ProdutoController *produtoCtrl = new ProdutoController();
	bool removido = produtoCtrl->removerProduto(id);

	if (removido == true) {
		cout << "Produto removido com sucesso" << endl;
	}
	else {
		cout << "Sem sucesso a remo��o do produto" << endl;
	}


}

void ProdutoView::consultaProdutos()
{
	ProdutoController *produtoctrl = new ProdutoController();
	vector<string> listaProdutos = produtoctrl->consultarProdutos();
	for (int i = 0; i < listaProdutos.size(); i++) {
		cout << listaProdutos[i] << endl;
	}
}

Produto* ProdutoView::insereProduto()
{
	string nome, descricao, dataVencimento, id, quantidade, preco;
	Produto *produto = new Produto();
	cin.clear();
	cout << "Id: ";
	cin.ignore();
	getline(cin, id);
	cin.clear();
	cout << endl;
	cout << "Nome: " << endl;
	getline(cin, nome);
	cin.clear();
	cout << "Descricao: " << endl;
	getline(cin, descricao);
	cin.clear();
	cout << "Quantidade: " << endl;
	getline(cin, quantidade);
	cin.clear();
	cout << "Preco: " << endl;
	getline(cin, preco);
	cin.clear();
	cout << "Data vencimento: " << endl;
	getline(cin, dataVencimento);
	cin.clear();

	produto->setId(id);
	produto->setNome(nome);
	produto->setDescricao(descricao);
	produto->setQuantidade(quantidade);
	produto->setPreco(preco);
	produto->setDataVencimento(dataVencimento);

	return produto;
}



