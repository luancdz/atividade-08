#pragma once
#include "Produto.h"

using namespace std;
class ProdutoView
{
public:
	ProdutoView();
	~ProdutoView();
	void menu();
	void removerProduto();
	void consultaProdutos();
	Produto* insereProduto();
};

