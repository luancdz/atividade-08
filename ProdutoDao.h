#pragma once
#include <iostream>
#include "Produto.h"
#include <fstream>
#include <vector>

using namespace std;
class ProdutoDao
{
private:
	string nomaArquivoTxt;
public:
	ProdutoDao();
	bool insertProduto(Produto *produto);
	vector<string> consultarProduto();
	bool removeProduto(string id);
	bool alteraProduto(string id, Produto *produto);
	~ProdutoDao();
	ofstream saida;
	ifstream entrada;
	void setNomeArquivo(string nome);
	string getNomeArquivo();

};

