#include "stdafx.h"
#include "ProdutoDao.h"
#include <vector>

using namespace std;
ProdutoDao::ProdutoDao()
{
	this->nomaArquivoTxt = "arquivo.txt";
}

bool ProdutoDao::insertProduto(Produto * produto)
{
	saida.open(getNomeArquivo(), ios_base::app);

	//Checa se abriu corretamente
	if (!saida.is_open()) {
		throw "Ocorreu um erro ao abrir o arquivo produto.txt, abortando....";
		return false;
	}
	//Salva em disco
	saida << produto->getId() << endl;
	saida << "Nome: " << produto->getNome() << endl;
	saida << "Descricao: " << produto->getDescricao() << endl;
	saida << "Quantidade: "<< produto->getQuantidade() << endl;
	saida << "Preco: " << produto->getPreco() << endl;
	saida << "Data Vencimento: " <<produto->getDataVencimento() << endl;

	//fecha
	saida.close();
	return true;
}

vector<string> ProdutoDao::consultarProduto()
{
	entrada.open(getNomeArquivo());
	entrada.clear();//Limpa a flag do EOF
	entrada.seekg(0, ios::beg);//Volta ao come�o do arquivo
	if (!entrada.is_open()) {
		throw "Ocorreu um erro ao abrir o arquivo produto.txt, abortando....";
	}
	vector<string> temp;
	string temporario;

	while (!entrada.eof()) {
		entrada >> temporario;//Pega o produto e joga num tempor�rio
		if (entrada.eof())
			break;
		//Faz uma copia deste tempor�rio num vetor
		temp.push_back(temporario);
	}

	return temp;
}

/*vector<string> ProdutoDao::consultarProduto()
{
		entrada.open(getNomeArquivo());
		entrada.clear();//Limpa a flag do EOF
		entrada.seekg(0, ios::beg);//Volta ao come�o do arquivo
		if (!entrada.is_open()) {
			throw "Ocorreu um erro ao abrir o arquivo produto.txt, abortando....";
		}
		vector<string> temp;
		string temporario;

		while (!entrada.eof()) {
			entrada >> temporario;//Pega o produto e joga num tempor�rio
			if (entrada.eof())
				break;
			//Faz uma copia deste tempor�rio num vetor
			temp.push_back(temporario);
		}

		return temp;
}*/

bool ProdutoDao::removeProduto(string id)
{
	//Le e separa em um vetor para remover dps o id selecionado
	vector<string> itensTexto;
	string line;
	ifstream arquivoLer("arquivo.txt");
	if (arquivoLer.is_open()) {
		while (getline(arquivoLer, line)) {
			itensTexto.push_back(line);
		}
		arquivoLer.close();
	}
	else {
		cout << "Erro ao abrir arquivo.";
	}

	//Adiciona todos os itens novamente, menos o do id selecionado.
	ofstream arquivo;
	arquivo.open("arquivo.txt");
	for (int i = 0; i<itensTexto.size(); i++) {
		if (itensTexto.at(i) == id) {
			i = i + 5;
		}
		else {
			arquivo << itensTexto.at(i);
			arquivo << "\n";
		}
	}
	arquivo.close();
	return true;
}


bool ProdutoDao::alteraProduto(string id, Produto *produto)
{
	vector<string> itensTexto;
	string line;
	ifstream arquivoLer("arquivo.txt");
	if (arquivoLer.is_open()) {
		while (getline(arquivoLer, line)) {
			itensTexto.push_back(line);
		}
		arquivoLer.close();
	}
	else {
		cout << "Erro ao abrir arquivo.";
		return false;
	}

	//Adiciona todos os itens novamente, menos o do id selecionado.
	ofstream arquivo;
	arquivo.open("arquivo.txt");
	for (int i = 0; i<itensTexto.size(); i++) {
		if (itensTexto.at(i) == id) {
			itensTexto.at(i) = produto->getId();
			arquivo <<itensTexto.at(i);
			arquivo << "\n";
			itensTexto.at(i+1) =produto->getNome();
			arquivo <<"Nome: " <<itensTexto.at(i +1);
			arquivo << "\n";
			itensTexto.at(i + 2) = produto->getDescricao();
			arquivo <<"Descricao: " <<itensTexto.at(i +2);
			arquivo << "\n";
			itensTexto.at(i + 3) = produto->getQuantidade();
			arquivo <<"Quantidade: " <<itensTexto.at(i +3 );
			arquivo << "\n";
			itensTexto.at(i + 4) = produto->getPreco();
			arquivo <<"Preco: " <<itensTexto.at(i + 4);
			arquivo << "\n";
			itensTexto.at(i + 5) = produto->getDataVencimento();
			arquivo <<"Data vencimento: " <<itensTexto.at(i + 5);
			arquivo << "\n";
			i = i + 5;
		}
		else {
			arquivo << itensTexto.at(i);
			arquivo << "\n";
		}
	}
	arquivo.close();
	return true;
}


ProdutoDao::~ProdutoDao()
{
}

void ProdutoDao::setNomeArquivo(string nome)
{
	this->nomaArquivoTxt = nome;
}

string ProdutoDao::getNomeArquivo()
{
	return this->nomaArquivoTxt;
}
